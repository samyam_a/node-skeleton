var express = require('express');
var path = require('path');
var config = require('./config');
var favicon = require('serve-favicon');
var log = require('./core/logger');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressPath = require('express-path');
var routes = require('./routes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('config', config);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(function (req, res, next) {
    req.config = app.get('config');
    next();
});
app.use(function (req, res, next) {
    log.info("Request Received: %s %s", req.method, req.url);
    req.log = log;
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

expressPath(app, routes, {verbose: false});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  log.error({method: req.method, url: req.url, message: err.message});
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
