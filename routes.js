exports = module.exports = [
    ['/',
        'index#index',
        'GET'],

    ['/api/v1/hello_world',
        'index#helloWorld',
        'auth#authenticated',
        'GET']
];
