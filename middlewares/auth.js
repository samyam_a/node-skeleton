var _ = require('lodash');
var AuthFailedException = require("../errors/error-types").AuthFailedException;

exports.authenticated = function(req, res, next){
    var config = req.app.get('config');
    var dummyAuth = true;

    if(!dummyAuth){
        var error = new AuthFailedException("Forbidden Invalid Key");
        next(error);
    }

    next();
};
