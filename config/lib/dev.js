var _ = require('lodash');
var config = {}

_.set(config, "db.main", {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mydb_dev'
});

exports = module.exports = config;
