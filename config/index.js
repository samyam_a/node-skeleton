var _   = require('lodash'),
defaults = {};

_.set(defaults, "db", {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'mydb'
});

function __getEnvConfig(){
    switch(process.env.NODE_ENV){
        case 'production':
            return require('./lib/prod');
        case 'staging':
            return require('./lib/staging');
        case 'development':
            return require('./lib/dev');
        default:
            return {};
    }
}

config = {};
exports = module.exports = _.defaultsDeep(config, __getEnvConfig(), defaults);