
function index(req, res, next) {
    res.render('index',{title: 'Hello World'});
};

function helloWorld(req, res, next) {
    res.send({msg: 'Hello World!'});
};

module.exports = {
    index: index,
    helloWorld: helloWorld
}
